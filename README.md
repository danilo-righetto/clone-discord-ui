# Criando o clone do Discord
Aprendendo com mais um vídeo da `Rocketseat` estou criando o clone do [Discord](https://discord.com/new) utilizando `ReactJS` com `typescript`.

## Estrutura base

Na aplicação utilizamos a biblioteca `styled-components` para a estilização dos nossos componentes.

## Configuração da IDE

A **IDE** utilizada para o desenvolvimento dessa aplicação é o [Visual Studio Code](https://code.visualstudio.com/).

### Temas

O tema utilizado é o [Dracula Official](https://marketplace.visualstudio.com/items?itemName=titouan.theme-dracula)

### Fontes

A melhor fonte para ser utilizada no ambiente de desenvolvimento é a [Fira Code](https://github.com/tonsky/FiraCode).
Baixe e instale no seu sistema operacional.

### Plugins
- [Color Highlight](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)
- [DotENV](https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv)
- [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [GraphQL](https://marketplace.visualstudio.com/items?itemName=Prisma.vscode-graphql)
- [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- [Live Share](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare)
- [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)
- [vscode-styled-components](https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components)
- [Rocketseat ReactJS](https://marketplace.visualstudio.com/items?itemName=rocketseat.RocketseatReactJS)
- [Rocketseat React Native](https://marketplace.visualstudio.com/items?itemName=rocketseat.RocketseatReactNative)

### Configurações

Acesse as [configurações](https://gitlab.com/snippets/1995661) para uma melhor experiência com a sua IDE.

## Informações adicionais

**Author do projeto**: [Guilherme Rodz](https://github.com/guilhermerodz)

**Repositório**: [youtube-clone-discord](https://github.com/Rocketseat/youtube-clone-discord)

**Aula**: [Recriando a Interface do Discord com ReactJS | UI Clone](https://www.youtube.com/watch?v=x4FdZd2-_uU)
